# alta3research-gitlab

Alta3 Research Git and GitLab Certification

## Description
Right now this is a template for learning gitlab and its available features and services.

## Usage
```sh
python main.py
```

## Support
Go get help.

## Roadmap
I have no ideas about the future.

## Contributing
Help figure out pull requests!!

## Authors and acknowledgment
Alta3 Research has contributed all knowledge into making this project.

## License
The content of this repo is ...probably... open source.

## Project status
Development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

